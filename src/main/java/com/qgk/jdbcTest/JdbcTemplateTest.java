package com.qgk.jdbcTest;

import com.qgk.bean.Account;
import com.qgk.dao.AccountDao;
import com.qgk.dao.impl.AccountDaoImpl;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

public class JdbcTemplateTest {
    //使用 execute() 方法建账户表
    @Test
    public void createDB() {
        //加载配置文件
       ApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");
        //获取JdbcTemplate实例
        JdbcTemplate jdbcTemplate = (JdbcTemplate) applicationContext.getBean("jdbcTemplate");
        //  执行SQL语句,建立用户账目管理表
        jdbcTemplate.execute(
             "create table account(id int primary key auto_increment, username varchar(50), balance double )"
                           );
        System.out.println("用户账户表建立成功");
    }

    @Test
    public void addAccountTest() {
        //加载配置文件
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");
        //D
        AccountDao accountDao = (AccountDao) applicationContext.getBean("accountDao");

        Account account = new Account();
        account.setUsername("liming");
        account.setBalance(3000);

        int i = accountDao.addAccount(account);


        if (i > 0) {
            System.out.println("成功插入"+i+"条数据");
        }else {
            System.out.println("对不起,数据插入失败");
        }

    }
    @Test
    public void updateAccountTest(){
        //加载配置文件
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");
        //D
        AccountDao accountDao = (AccountDao) applicationContext.getBean("accountDao");

        Account account = new Account();
        account.setId(1);
        account.setUsername("liming");
        account.setBalance(2100.00);

        int i = accountDao.updateAccount(account);


        if (i > 0) {
            System.out.println("成功更新"+i+"条数据");
        }else {
            System.out.println("对不起,数据更新失败");
        }
    }

}
