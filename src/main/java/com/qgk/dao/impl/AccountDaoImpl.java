package com.qgk.dao.impl;

import com.qgk.bean.Account;
import com.qgk.dao.AccountDao;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class AccountDaoImpl implements AccountDao {
    // 声明JdbcTemplate属性及其setter方法
    private JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
    //添加用户
    @Override

    public int addAccount(Account account) {
        //定义SQL
        String sql = " insert into account(username,balance) values(?,?)";
        // 定义数组来存放SQL语句中的参数
        Object[] object = new Object[]{account.getUsername(), account.getBalance()};
        //
        // 执行添加操作，返回的是受SQL语句影响的记录条数
        int i = this.jdbcTemplate.update(sql, object);
        return i;
    }




    public int updateAccount(Account account) {
        // 定义更新操作SQL
        String sql = "update account set username=?, balance = ? where id = ?";
        // 定义数组来存放SQL语句中的参数
        Object[]   params = new Object[]{account.getUsername(), account.getBalance(),account.getId()};
        //// 执行更新操作，返回的是受SQL语句影响的记录条数
        int num = this.jdbcTemplate.update(sql, params);
        return num;
    }/**/

    @Override
    public int deleteAccount(int id) {
        return 0;
    }
}
