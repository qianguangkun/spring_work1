package com.qgk.services;

public interface AccountService {
    public void transfer(String outUser,String inUser, Double money);
}
