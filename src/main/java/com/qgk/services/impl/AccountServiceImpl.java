package com.qgk.services.impl;


import org.springframework.jdbc.core.JdbcTemplate;
import com.qgk.services.AccountService;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class AccountServiceImpl implements AccountService {
    private JdbcTemplate jdbcTemplate;
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
    @Transactional(propagation = Propagation.REQUIRED,
            isolation = Isolation.DEFAULT, readOnly = false)
    public void transfer(String outUser,String inUser, Double money){
        //收款方
        this.jdbcTemplate.update("update account set balance = balance + ? where username = ?", money, inUser);
        //欠钱方
        this.jdbcTemplate.update("update account set balance = balance - ? where username = ?", money, outUser);
    }


}
